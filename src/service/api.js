import Axios from 'axios';

class Api {
  constructor () {
    this.axios = Axios.create();
  }

  getGroupsByPrograms() {
    return this.axios.get('/programs.json').then(response => response.data.result);
  }
}

export default new Api();
